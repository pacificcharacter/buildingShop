package com.buildingshop.domains.enums;

public enum UserRolesEnum {

    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MODERATOR,
    ROLE_SUPER_ADMIN;

}
