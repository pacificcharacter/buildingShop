package com.buildingshop.domains.violations;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;

@NotNull(message = " cannot be empty")
@Pattern(regexp = "[0-9]*", message = " cannot contains alphabetic characters")
@Size(min = 10, max = 11, message = " is incorrect")
@Constraint(validatedBy = {})
@Target({METHOD, FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface PhoneNumber {
    String message() default " is incorrect";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
