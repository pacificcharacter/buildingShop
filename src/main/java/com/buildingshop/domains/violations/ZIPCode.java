package com.buildingshop.domains.violations;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Size(min = 5, max = 6, message = " must contains 5 or 6 symbols")
@Pattern(regexp = "[0-9]*", message = " cannot contains alphabetic characters")
@Constraint(validatedBy = {})
@Target({METHOD, FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ZIPCode {
    String message() default " is incorrect";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
