package com.buildingshop.domains.violations;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@NotNull(message = " cannot be empty")
@Size(min = 1, max = 50, message = " must be between {min} and {max}")
@Pattern(regexp = "[a-zA-Z]*", message = " cannot contains alphabetic characters")
@Constraint(validatedBy = {})
@Target({METHOD, FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface FirstName {
    String message() default " is incorrect";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
