package com.buildingshop.domains;

import com.buildingshop.controllers.utils.implementation.DataNormalizerImpl;
import com.buildingshop.domains.violations.FirstName;
import com.buildingshop.domains.violations.LastName;
import com.buildingshop.domains.violations.PhoneNumber;
import com.buildingshop.domains.violations.ZIPCode;
import com.google.common.base.Objects;
import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USERS2")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true)
    private Long userID;

    @NotNull(message = " cannot be empty")
    @Email(message = " is incorrect")
    @Size(min = 7, max = 40, message = " may be between {min} and {max} symbols")
    @Column(name = "EMAIL", nullable = false, unique = true, length = 40)
    private String userEmail;

    @NotNull
    @Size(min = 6, max = 60)
    @Column(name = "PASSWORD", nullable = false, length = 60)
    private String userPassword;

    @Transient
    private String purePassword;

    @FirstName
    @Column(name = "FIRSTNAME", nullable = false, length = 50)
    private String userFirstName;

    @LastName
    @Column(name = "LASTNAME", nullable = false, length = 50)
    private String userLastName;

    @PhoneNumber
    @Column(name = "PHONE_NUMBER", nullable = false, unique = true, length = 11)
    private String userPhoneNumber;

    @ZIPCode
    @Column(name = "ZIP_CODE", length = 6)
    private String userZIPCode;

    @Column(name = "DATE_OF_REG", nullable = false)
    @Past(message = " time error")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar calendar;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLE1", joinColumns = {
            @JoinColumn(name = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID")},
            uniqueConstraints = {@UniqueConstraint(columnNames = {"ID", "ROLE_ID"})})
    private Set<UserRole> userRole = new HashSet<>(0);

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserZIPCode() {
        return userZIPCode;
    }

    public void setUserZIPCode(String userZIPCode) {
        this.userZIPCode = userZIPCode;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public String getPurePassword() {
        return purePassword;
    }

    public void setPurePassword(String purePassword) {
        this.purePassword = purePassword;
    }

    public Set<UserRole> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<UserRole> userRole) {
        this.userRole = userRole;
    }

    public String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return format.format(calendar.getTime());
    }

    public void setDate(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            this.calendar = Calendar.getInstance();
            calendar.setTime(format.parse(date));
        } catch (Exception e) {
            this.calendar = Calendar.getInstance();
        }
    }

    @PrePersist
    public void prePersist() {
        purePassword = this.userPassword;
        this.userPassword = new BCryptPasswordEncoder().encode(this.userPassword);
        this.calendar = Calendar.getInstance();
        this.userFirstName = new DataNormalizerImpl().toUpperCaseFirstChar(userFirstName);
        this.userLastName = new DataNormalizerImpl().toUpperCaseFirstChar(userLastName);
    }

    public void preUpdate() {
        this.userPassword = new BCryptPasswordEncoder().encode(this.userPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUserID());
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || getClass() != object.getClass()) return false;
        if (this == object) return true;
        User thisUser = (User) object;
        return Objects.equal(getUserID(), thisUser.getUserID());
    }
}
