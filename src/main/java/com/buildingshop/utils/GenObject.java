package com.buildingshop.utils;

public class GenObject<T> {

    private T type;

    public GenObject() {}

    public GenObject(T type) {
        this.type = type;
    }

    public T getInstance() {
        return type;
    }

    public void setInstance(T type) {
        this.type = type;
    }
}
