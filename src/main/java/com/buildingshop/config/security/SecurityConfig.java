package com.buildingshop.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.authorizeRequests()
                .antMatchers("/account").authenticated()
                .antMatchers("/account/**").access("hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_SUPER_ADMIN')")
                .antMatchers("/search/**").access("hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_SUPER_ADMIN')")
                .antMatchers("/admin/**").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");

        httpSecurity.exceptionHandling()
                .accessDeniedPage("/403");

        httpSecurity.sessionManagement()
                .maximumSessions(1)
                .expiredUrl("/")
                .sessionRegistry(sessionRegistry());

        httpSecurity.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/resources/**", "/**").permitAll()
                .anyRequest().permitAll()
                .and();

        httpSecurity.formLogin()
                .loginPage("/auth")
                .loginProcessingUrl("/j_spring_security_check")
                .failureUrl("/?error")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .defaultSuccessUrl("/account")
                .permitAll();

        httpSecurity.logout()
                .permitAll()
                .logoutUrl("/account/logout")
                .logoutSuccessUrl("/?logout")
                .invalidateHttpSession(true);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean(name = "sessionRegistry")
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}
