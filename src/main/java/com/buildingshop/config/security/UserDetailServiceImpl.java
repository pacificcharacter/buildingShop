package com.buildingshop.config.security;

import com.buildingshop.domains.UserRole;
import com.buildingshop.domains.User;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.ejb.EJB;
import java.util.HashSet;
import java.util.Set;

@Service("userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {
        User user = userService.getUser("userEmail", new GenObject<>(userEmail));
        Set<GrantedAuthority> authoritySet = buildUserAuthority(user.getUserRole());
        return buildUserForAuthentication(user, authoritySet);
    }

    private org.springframework.security.core.userdetails.User
    buildUserForAuthentication(User user, Set<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getUserEmail(),
                user.getUserPassword(), true, true, true, true, authorities);
    }

    private Set<GrantedAuthority> buildUserAuthority(Set<UserRole> roles) {

        Set<GrantedAuthority> authoritySet = new HashSet<>();
        for (UserRole userUserRole : roles) {
            authoritySet.add(new SimpleGrantedAuthority(userUserRole.getRole()));
        }
        return authoritySet;
    }
}
