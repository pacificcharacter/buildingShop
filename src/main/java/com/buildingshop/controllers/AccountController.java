package com.buildingshop.controllers;

import com.buildingshop.controllers.utils.interfaces.SuperUserVerification;
import com.buildingshop.domains.User;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import java.security.Principal;

@Controller
@RequestMapping("/account")
public class AccountController {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    @Inject
    @Named("superUserVerify")
    private SuperUserVerification superUserVerification;

    @RequestMapping(method = RequestMethod.GET)
    public String getUserId(Principal principal, Model model) {
        model.addAttribute(userService.getUser("userEmail", new GenObject<>(principal.getName())));
        return "account";
    }

    @RequestMapping(value = "/{userID}", method = RequestMethod.GET)
    public String showUserProfile(@PathVariable String userID, Model model, Principal principal) {
        if (userID.matches("[0-9]*")) {
            if (userService.isExists("userID", new GenObject<>(userID))) {
                User user = userService.getUser("userID", new GenObject<>(userID));
                if (superUserVerification.isSuperUser(user, principal.getName())) {
                    return "403";
                }
                model.addAttribute(user);
                return "account";
            }
        }
        return "404";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        return "redirect:/?logout";
    }
}
