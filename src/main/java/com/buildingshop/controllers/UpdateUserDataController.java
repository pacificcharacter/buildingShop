package com.buildingshop.controllers;

import com.buildingshop.controllers.utils.implementation.Views;
import com.buildingshop.controllers.utils.interfaces.AjaxResponseBody;
import com.buildingshop.controllers.utils.interfaces.DataNormalizer;
import com.buildingshop.controllers.utils.interfaces.RegisterVerification;
import com.buildingshop.domains.User;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import org.codehaus.jackson.map.annotate.JsonView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

@Controller
public class UpdateUserDataController {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    @Inject
    @Named("verification")
    private RegisterVerification registerVerification;

    @Inject
    @Named("normalizer")
    private DataNormalizer normalizer;

    @Inject
    @Named("ajaxResponseBody")
    private AjaxResponseBody responseBody;

    @ResponseBody
    @JsonView(Views.Public.class)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public AjaxResponseBody updateUser(@RequestParam("email") String email,
                                       @RequestParam("name") String name,
                                       @RequestParam("lastName") String lastName,
                                       @RequestParam("zip") String zipCode) {

        User user = userService.getUser("userEmail", new GenObject<>(email.trim()));
        user.setUserFirstName(normalizer.toUpperCaseFirstChar(name));
        user.setUserLastName(normalizer.toUpperCaseFirstChar(lastName));
        user.setUserZIPCode(zipCode);
        userService.updateUser(user);

        responseBody.setCode("200");
        responseBody.setMsg("user updated");

        return responseBody;
    }

    @RequestMapping(value = "/oldPasswordValidation", method = RequestMethod.POST)
    public void checkPassword(HttpServletResponse response,
                              @RequestParam("password") String password,
                              @RequestParam("email") String email) throws Exception {
        registerVerification.checkCurrentPassword(response, email, password);
    }

    @ResponseBody
    @JsonView(Views.Public.class)
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public AjaxResponseBody changePassword(@RequestParam("password") String newPassword,
                                           @RequestParam("email") String email) {

        User user = userService.getUser("userEmail", new GenObject<>(email.trim()));
        if (!newPassword.trim().isEmpty()) user.setUserPassword(newPassword);
        user.preUpdate();
        userService.updateUser(user);
        responseBody.setCode("200");
        responseBody.setMsg("password updated");
        return responseBody;
    }
}
