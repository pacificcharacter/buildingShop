package com.buildingshop.controllers.utils.implementation;

import com.buildingshop.config.EmailSenderConfig;
import com.buildingshop.controllers.utils.interfaces.EmailSender;
import com.buildingshop.domains.User;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Named("emailSender")
public class EmailSenderImpl implements EmailSender {

    @Inject
    @Named("emailSenderConfig")
    private EmailSenderConfig emailSenderConfig;

    public void sendEmail(User user, String newPassword, String subject) throws MessagingException {
        MimeMessage mimeMessage = emailSenderConfig.javaMailSender().createMimeMessage();
        MimeMessageHelper mailMassage = new MimeMessageHelper(mimeMessage);
        mailMassage.setFrom("netcrackersender@gmail.com");
        mailMassage.setTo(user.getUserEmail());
        mailMassage.setSubject(subject);
        mailMassage.setText("Your login: " + user.getUserEmail() +
                "\nNever tell anyone your password: " + newPassword);
        emailSenderConfig.javaMailSender().send(mimeMessage);
    }

}
