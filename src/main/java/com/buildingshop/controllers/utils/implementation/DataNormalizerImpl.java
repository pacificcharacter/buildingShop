package com.buildingshop.controllers.utils.implementation;

import com.buildingshop.controllers.utils.interfaces.DataNormalizer;

import javax.inject.Named;

@Named("normalizer")
public class DataNormalizerImpl implements DataNormalizer {

    public DataNormalizerImpl() {
    }

    public String toUpperCaseFirstChar(String string) {
        return string.toLowerCase().replaceFirst("[a-z]{1}", string.substring(0, 1).toUpperCase());
    }
}
