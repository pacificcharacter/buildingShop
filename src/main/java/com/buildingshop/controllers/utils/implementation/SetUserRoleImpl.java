package com.buildingshop.controllers.utils.implementation;

import com.buildingshop.controllers.utils.interfaces.ExpiringSession;
import com.buildingshop.controllers.utils.interfaces.SetUserRole;
import com.buildingshop.domains.User;
import com.buildingshop.domains.UserRole;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import org.springframework.security.access.annotation.Secured;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashSet;
import java.util.Set;

@Named("setUserRole")
public class SetUserRoleImpl implements SetUserRole {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    @Inject
    @Named("expiringSession")
    private ExpiringSession expiringSession;

    @Secured("ROLE_ADMIN")
    public boolean demote(Long userID, String role) {

        User user = userService.getUser("userID", new GenObject<>(userID));
        Set<UserRole> roles = new HashSet<>();
        roles.add(new UserRole(role));
        user.setUserRole(roles);
        userService.updateUser(user);
        expiringSession.expireSession(user);

        return true;
    }

    @Secured("ROLE_ADMIN")
    public boolean addUserRole(Long userID, String role) {

        boolean isSuccess = false;
        User user = userService.getUser("userID", new GenObject<>(userID));
        Set<UserRole> userRoleIterator = user.getUserRole();

        for (UserRole userRole : userRoleIterator) {
            if (role.equals(userRole.getRole())) {
                user.getUserRole().add(new UserRole(role));
                userService.updateUser(user);
                expiringSession.expireSession(user);

                isSuccess = true;
            }
        }
        return isSuccess;
    }
}
