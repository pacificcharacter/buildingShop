package com.buildingshop.controllers.utils.implementation;

import com.buildingshop.controllers.utils.interfaces.ExpiringSession;
import com.buildingshop.domains.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named("expiringSession")
public class ExpiringSessionImpl implements ExpiringSession {

    @Autowired
    @Named("sessionRegistry")
    private SessionRegistry sessionRegistry;

    public List<SessionInformation> getActiveSessions() {
        List<SessionInformation> activeSession = new ArrayList<>();
        for (Object principal : sessionRegistry.getAllPrincipals()) {
            activeSession.addAll(sessionRegistry.getAllSessions(principal, false));
        }
        return activeSession;
    }

    public void expireSession(User user) {
        List<SessionInformation> sessionInformation = getActiveSessions();
        for (SessionInformation session : sessionInformation) {
            Object principal = session.getPrincipal();
            org.springframework.security.core.userdetails.User springUser =
                    (org.springframework.security.core.userdetails.User) principal;
            if (user.getUserEmail().equals(springUser.getUsername())) {
                session.expireNow();
            }
        }
    }
}
