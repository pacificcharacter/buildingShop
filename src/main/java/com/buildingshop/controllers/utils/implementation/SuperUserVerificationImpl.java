package com.buildingshop.controllers.utils.implementation;

import com.buildingshop.controllers.utils.interfaces.SuperUserVerification;
import com.buildingshop.domains.User;
import com.buildingshop.domains.UserRole;
import com.buildingshop.domains.enums.UserRolesEnum;

import javax.inject.Named;
import java.util.Set;

@Named("superUserVerify")
public class SuperUserVerificationImpl implements SuperUserVerification {

    public boolean isSuperUser(User user, String principalName) {
        boolean valid = false;
        Set<UserRole> userRole = user.getUserRole();
        for (UserRole role : userRole) {
            if (role.getRole().equals(UserRolesEnum.ROLE_SUPER_ADMIN.name()) &&
                    !principalName.equals(user.getUserEmail())) {
                valid = true;
            }
        }
        return valid;
    }
}
