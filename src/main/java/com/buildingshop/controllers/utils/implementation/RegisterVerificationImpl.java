package com.buildingshop.controllers.utils.implementation;

import com.buildingshop.controllers.utils.interfaces.RegisterVerification;
import com.buildingshop.domains.User;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import com.google.gson.Gson;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Named("verification")
public class RegisterVerificationImpl implements RegisterVerification {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public RegisterVerificationImpl() {
    }

    private void write(HttpServletResponse response, Map<String, Object> map) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(new Gson().toJson(map));
    }

    public void checkEmail(HttpServletResponse response, String email) throws IOException {

        Map<String, Object> map = new HashMap<>();
        boolean isValid = false;
        email = email.trim();

        if (!email.isEmpty()) {

            Set<ConstraintViolation<User>> violations = validator.validateValue(User.class, "userEmail", email);

            if (violations.size() == 0) {
                GenObject<String> userEmail = new GenObject<>(email);
                if (!userService.isExists("userEmail", userEmail)) {
                    isValid = true;
                    map.put("email", "is valid");
                }
            } else map.put("email", violations.iterator().next().getMessage());
        }
        map.put("isValid", isValid);
        write(response, map);
    }

    public void checkPassword(HttpServletResponse response, String password) throws IOException {

        Map<String, Object> map = new HashMap<>();
        boolean isValid = false;
        password = password.trim();

        if (!password.isEmpty()) {

            Set<ConstraintViolation<User>> violations = validator.validateValue(User.class, "userPassword", password);

            if (violations.size() == 0) {
                isValid = true;
                map.put("password", "is valid");
            } else {
                String trouble = violations.iterator().next().getMessage();
                map.put("password", trouble);
            }
        }
        map.put("isValid", isValid);
        write(response, map);
    }

    public void checkName(HttpServletResponse response, String firstName) throws IOException {

        Map<String, Object> map = new HashMap<>();
        boolean isValid = false;
        firstName = firstName.trim();

        if (!firstName.isEmpty()) {

            Set<ConstraintViolation<User>> violations = validator.validateValue(User.class, "userFirstName", firstName);

            if (violations.size() == 0) {
                isValid = true;
                map.put("name", "is valid");
            } else map.put("name", violations.iterator().next().getMessage());

        }
        map.put("isValid", isValid);
        write(response, map);
    }

    public void checkLastName(HttpServletResponse response, String lastName) throws IOException {

        Map<String, Object> map = new HashMap<>();
        boolean isValid = false;
        lastName = lastName.trim();

        if (!lastName.isEmpty()) {

            Set<ConstraintViolation<User>> violations = validator.validateValue(User.class, "userLastName", lastName);

            if (violations.size() == 0) {
                isValid = true;
                map.put("lastname", "is valid");
            } else map.put("lastname", violations.iterator().next().getMessage());
        }
        map.put("isValid", isValid);
        write(response, map);
    }

    public void checkZIPCode(HttpServletResponse response, String zipCode) throws IOException {

        Map<String, Object> map = new HashMap<>();
        boolean isValid = false;
        zipCode = zipCode.trim();

        if (!zipCode.isEmpty()) {

            Set<ConstraintViolation<User>> violations = validator.validateValue(User.class, "userZIPCode", zipCode);

            if (violations.size() == 0) {
                isValid = true;
                map.put("zip", "is valid");
            } else map.put("zip", violations.iterator().next().getMessage());
        }
        map.put("isValid", isValid);
        write(response, map);
    }

    public void checkPhoneNumber(HttpServletResponse response, String phoneNumber) throws IOException {

        Map<String, Object> map = new HashMap<>();
        boolean isValid = false;
        phoneNumber = phoneNumber.trim();

        if (!phoneNumber.isEmpty()) {

            Set<ConstraintViolation<User>> violations = validator.validateValue(User.class, "userPhoneNumber", phoneNumber);

            if (violations.size() == 0) {
                GenObject<String> userPhoneNumber = new GenObject<>(phoneNumber);

                if (!userService.isExists("userPhoneNumber", userPhoneNumber)) {
                    isValid = true;
                    map.put("phone", "is valid");
                }
            } else map.put("phone", violations.iterator().next().getMessage());
        }
        map.put("isValid", isValid);
        write(response, map);
    }

    public void checkCurrentPassword(HttpServletResponse response, String email, String password) throws IOException {

        Map<String, Object> map = new HashMap<>();
        boolean isValid = false;
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if (passwordEncoder.matches(password,
                userService.getUser("userEmail", new GenObject<>(email)).getUserPassword()))
            isValid = true;

        map.put("isValid", isValid);
        write(response, map);
    }
}
