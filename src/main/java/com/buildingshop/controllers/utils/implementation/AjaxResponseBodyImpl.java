package com.buildingshop.controllers.utils.implementation;

import com.buildingshop.controllers.utils.interfaces.AjaxResponseBody;
import com.buildingshop.domains.User;
import org.codehaus.jackson.map.annotate.JsonView;

import javax.inject.Named;
import java.util.List;

@Named("ajaxResponseBody")
public class AjaxResponseBodyImpl implements AjaxResponseBody {

    @JsonView(Views.Public.class)
    private String msg;

    @JsonView(Views.Public.class)
    private String code;

    @JsonView(Views.Public.class)
    private List<User> result;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setResult(List<User> users) {
        this.result = users;
    }

    public List<User> getResult() {
        return this.result;
    }
}