package com.buildingshop.controllers.utils.interfaces;

import com.buildingshop.domains.User;

import javax.mail.MessagingException;

public interface EmailSender {

    void sendEmail(User user, String newPassword, String subject) throws MessagingException;
}
