package com.buildingshop.controllers.utils.interfaces;

public interface SetUserRole {

    boolean demote(Long userID, String role);

    boolean addUserRole(Long userID, String role);

}
