package com.buildingshop.controllers.utils.interfaces;

import com.buildingshop.domains.User;
import org.springframework.security.core.session.SessionInformation;

import java.util.List;

public interface ExpiringSession {

    void expireSession(User user);

    List<SessionInformation> getActiveSessions();

}
