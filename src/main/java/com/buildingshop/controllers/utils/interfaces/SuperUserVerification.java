package com.buildingshop.controllers.utils.interfaces;

import com.buildingshop.domains.User;

public interface SuperUserVerification {

    boolean isSuperUser(User user, String principalName);
}
