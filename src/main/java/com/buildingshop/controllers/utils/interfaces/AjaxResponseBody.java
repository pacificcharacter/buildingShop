package com.buildingshop.controllers.utils.interfaces;

import com.buildingshop.domains.User;

import java.util.List;

public interface AjaxResponseBody {

    void setMsg(String msg);

    String getMsg();

    void setCode(String code);

    String getCode();

    void setResult(List<User> users);

    List<User> getResult();
}
