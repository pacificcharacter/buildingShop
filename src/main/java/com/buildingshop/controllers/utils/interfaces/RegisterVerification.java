package com.buildingshop.controllers.utils.interfaces;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface RegisterVerification {

    void checkEmail(HttpServletResponse response, String email) throws IOException;

    void checkPassword(HttpServletResponse response, String password) throws IOException;

    void checkName(HttpServletResponse response, String firstName) throws IOException;

    void checkLastName(HttpServletResponse response, String lastName) throws IOException;

    void checkZIPCode(HttpServletResponse response, String zipCode) throws IOException;

    void checkPhoneNumber(HttpServletResponse response, String phoneNumber) throws IOException;

    void checkCurrentPassword(HttpServletResponse response, String email, String password) throws IOException;
}
