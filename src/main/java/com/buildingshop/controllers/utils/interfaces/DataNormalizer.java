package com.buildingshop.controllers.utils.interfaces;

public interface DataNormalizer {

     String toUpperCaseFirstChar(String string);

}
