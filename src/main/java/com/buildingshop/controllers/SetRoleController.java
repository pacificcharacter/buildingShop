package com.buildingshop.controllers;

import com.buildingshop.controllers.utils.interfaces.SetUserRole;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.inject.Named;

@Controller
@RequestMapping(value = "/admin/setRole")
public class SetRoleController {

    @Inject
    @Named("setUserRole")
    private SetUserRole setUserRole;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody boolean setRole(@RequestParam("role") String role, @RequestParam("userID") Long userID) {
        boolean isSuccess = false;
        switch (role) {
            case "ROLE_USER": {
                isSuccess = setUserRole.demote(userID, role);
                break;
            }
            case "ROLE_MODERATOR": {
                isSuccess = setUserRole.addUserRole(userID, role);
                break;
            }
            case "ROLE_ADMIN": {
                isSuccess = setUserRole.addUserRole(userID, role);
                break;
            }
        }
        return isSuccess;
    }
}
