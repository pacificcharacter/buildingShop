package com.buildingshop.controllers;

import com.buildingshop.controllers.utils.implementation.DataNormalizerImpl;
import com.buildingshop.controllers.utils.interfaces.EmailSender;
import com.buildingshop.domains.User;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@Controller
public class SendingEmailController {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Inject
    @Named("emailSender")
    private EmailSender emailSenderImpl;

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
    public String forgotPassword() {
        return "rememberPassword";
    }

    @RequestMapping(value = "/sendPassword", method = RequestMethod.POST)
    public ModelAndView sendToMe(@RequestParam("email") String email, @RequestParam("name") String name,
                                 @RequestParam("phone") String phone) throws MessagingException {

        Set<ConstraintViolation<User>> violations = validator.validateValue(User.class, "userEmail", email);
        if (violations.size() == 0) {
            if (userService.isExists("userEmail", new GenObject<>(email))) {
                User user = userService.getUser("userEmail", new GenObject<>(email));
                if (user.getUserFirstName().equals(new DataNormalizerImpl().toUpperCaseFirstChar(name))
                        && user.getUserPhoneNumber().equals(phone)) {

                    String newPassword = RandomStringUtils.randomAlphanumeric(8);
                    user.setUserPassword(newPassword);
                    user.preUpdate();
                    userService.updateUser(user);
                    emailSenderImpl.sendEmail(user, newPassword, "Your new password");

                    return new ModelAndView("auth").addObject("msg", "Well done! Check your mail box.");
                }
            }
        }
        return new ModelAndView("rememberPassword").addObject("error", "Oh, user not found!");
    }
}
