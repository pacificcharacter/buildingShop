package com.buildingshop.controllers;

import com.buildingshop.controllers.utils.implementation.Views;
import com.buildingshop.controllers.utils.interfaces.AjaxResponseBody;
import com.buildingshop.controllers.utils.interfaces.RegisterVerification;
import com.buildingshop.domains.User;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import org.codehaus.jackson.map.annotate.JsonView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Controller
@RequestMapping(value = "/search")
public class UserSearchController {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    @Inject
    @Named("verification")
    private RegisterVerification registerVerification;

    @Inject
    @Named("ajaxResponseBody")
    private AjaxResponseBody responseBody;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView search() {
        return new ModelAndView("search", "users", userService.getAllUsers());
    }

    @RequestMapping(value = {"/delete"}, method = RequestMethod.POST)
    public
    @ResponseBody
    AjaxResponseBody delete(@RequestParam("userID") Long id) {
        userService.deleteUser(id);
        responseBody.setCode("200");
        responseBody.setMsg("user deleted");

        return responseBody;
    }

    @ResponseBody
    @JsonView(Views.Public.class)
    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public AjaxResponseBody returnUser(@RequestParam("expression") String expression,
                                       @RequestParam("criterion") String criterion) {
        switch (expression) {
            case "anything":
                responseBody.setCode("200");
                responseBody.setMsg("");
                responseBody.setResult(userService.getAllUsers());
                break;
            case "phone": {
                List<User> users = userService.getLike("userPhoneNumber", new GenObject<>(criterion));
                responseBody = isUserEmpty(users, responseBody);
                break;
            }
            case "email": {
                List<User> users = userService.getLike("userEmail", new GenObject<>(criterion));
                responseBody = isUserEmpty(users, responseBody);
                break;
            }
            default:
                responseBody.setCode("400");
                responseBody.setMsg("Search criteria is empty!");
                break;
        }
        return responseBody;
    }

    private AjaxResponseBody isUserEmpty(List<User> users, AjaxResponseBody responseBody) {
        if (!users.isEmpty()) {
            responseBody.setCode("200");
            responseBody.setMsg("");
            responseBody.setResult(users);
            return responseBody;
        }
        responseBody.setCode("204");
        responseBody.setMsg("No user!");

        return responseBody;
    }
}
