package com.buildingshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
@RequestMapping(value = "/auth")
public class AuthenticationController {

    @RequestMapping(method = RequestMethod.GET)
    public String returnAuth(Principal principal) {
        if (principal != null) {
            return "redirect:/account";
        }
        return "auth";
    }

}
