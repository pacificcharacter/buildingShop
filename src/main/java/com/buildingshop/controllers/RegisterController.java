package com.buildingshop.controllers;

import com.buildingshop.controllers.utils.interfaces.EmailSender;
import com.buildingshop.controllers.utils.interfaces.RegisterVerification;
import com.buildingshop.domains.User;
import com.buildingshop.domains.UserRole;
import com.buildingshop.domains.enums.UserRolesEnum;
import com.buildingshop.service.interfaces.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

@Controller
public class RegisterController {

    @EJB(mappedName = "java:module/UserServiceImpl")
    private UserService userService;

    @Inject
    @Named("verification")
    private RegisterVerification registerVerification;

    @Inject
    @Named("emailSender")
    private EmailSender emailSender;

    @RequestMapping(value = "/registerMe", method = RequestMethod.GET)
    public ModelAndView home(Principal principal) {
        if (principal != null) {
            return new ModelAndView("redirect:/account");
        }
        return new ModelAndView("registerPage", "user", new User());
    }

    @RequestMapping(value = "/emailValidation", method = RequestMethod.POST)
    public void checkEmail(HttpServletResponse response,
                           @RequestParam("email") String email) throws Exception {
        registerVerification.checkEmail(response, email);
    }

    @RequestMapping(value = "/passwordValidation", method = RequestMethod.POST)
    public void checkPassword(HttpServletResponse response,
                              @RequestParam("password") String password) throws Exception {
        registerVerification.checkPassword(response, password);
    }

    @RequestMapping(value = "/firstNameValidation", method = RequestMethod.POST)
    public void checkFirstName(HttpServletResponse response,
                               @RequestParam("firstName") String firstName) throws Exception {
        registerVerification.checkName(response, firstName);
    }

    @RequestMapping(value = "/lastNameValidation", method = RequestMethod.POST)
    public void checkLastName(HttpServletResponse response,
                              @RequestParam("lastName") String lastName) throws Exception {
        registerVerification.checkLastName(response, lastName);
    }

    @RequestMapping(value = "/zipCodeValidation", method = RequestMethod.POST)
    public void checkZIPCode(HttpServletResponse response,
                             @RequestParam("zipCode") String zipCode) throws Exception {
        registerVerification.checkZIPCode(response, zipCode);
    }

    @RequestMapping(value = "/phoneNumberValidation", method = RequestMethod.POST)
    public void checkPhoneNumber(HttpServletResponse response,
                                 @RequestParam("phoneNumber") String phoneNumber) throws Exception {
        registerVerification.checkPhoneNumber(response, phoneNumber);
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        return "redirect:/registerMe";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser(@Valid @ModelAttribute("user") User user, Errors errors) throws MessagingException {
        if (errors.hasErrors()) {
            return "registerPage";
        }
        Set<UserRole> userRoleSet = new HashSet<>();
        userRoleSet.add(new UserRole(UserRolesEnum.ROLE_USER.name()));
        user.setUserRole(userRoleSet);
        userService.registerUser(user);
        emailSender.sendEmail(user, user.getPurePassword(), "Welcome!");

        return "redirect:/auth";
    }
}
