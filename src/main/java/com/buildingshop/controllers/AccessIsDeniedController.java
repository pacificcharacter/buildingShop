package com.buildingshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/403")
public class AccessIsDeniedController {

    @RequestMapping(method = RequestMethod.GET)
    public String accessIsDenied() {
        return "403";
    }

}
