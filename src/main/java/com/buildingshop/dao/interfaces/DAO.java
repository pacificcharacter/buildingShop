package com.buildingshop.dao.interfaces;

import com.buildingshop.utils.GenObject;

import java.util.List;

public interface DAO<T> {

    List<T> getAll();

    void add(T object);

    T get(String expression, GenObject<?> criterion);

    void delete(Long userID);

    void delete(String expression, GenObject<?> criterion);

    List<T> getLike(String expression, GenObject<?> criterion);

    void update(T user);

    void update(T old, T newbie);

    boolean isExists(String expression, GenObject<?> criterion);
}
