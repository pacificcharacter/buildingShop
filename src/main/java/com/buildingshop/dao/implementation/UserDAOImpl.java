package com.buildingshop.dao.implementation;

import com.buildingshop.dao.interfaces.DAO;
import com.buildingshop.domains.User;
import com.buildingshop.utils.GenObject;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Named("userDAO")
public class UserDAOImpl implements DAO<User> {

    @PersistenceContext(unitName = "MySqlDS")
    private EntityManager entityManager;

    public List<User> getAll() {

        CriteriaQuery<User> criteria = entityManager.getCriteriaBuilder().createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root);
        return entityManager.createQuery(criteria).getResultList();
    }

    public void add(User user) {
        entityManager.persist(user);
    }

    public User get(String expression, GenObject<?> criterion) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> user = criteria.from(User.class);
        criteria.where(builder.equal(user.get(expression), criterion.getInstance()));
        List<User> users = entityManager.createQuery(criteria).getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    public void delete(String expression, GenObject<?> criterion) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        criteria.where(builder.equal(root.get(expression), criterion.getInstance()));
        List<User> users = entityManager.createQuery(criteria).getResultList();
        if (!users.isEmpty()) entityManager.remove(users.get(0));
    }

    public List<User> getLike(String expression, GenObject<?> criterion) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> user = criteria.from(User.class);
        criteria.where(builder.like(builder.lower(user.get(
                entityManager.getMetamodel().entity(User.class).
                        getDeclaredSingularAttribute(expression, String.class)
        )),"%"+ criterion.getInstance() + "%"));

        List<User> users = entityManager.createQuery(criteria).getResultList();
        if (users.isEmpty()) return null;
        return users;
    }

    public void delete(Long userID) {
        User userInstance = entityManager.find(User.class, userID);
        if (userInstance != null) entityManager.remove(userInstance);
    }

    public void update(User user) {
        entityManager.merge(user);
    }

    public void update(User oldUser, User newUser) {
        oldUser.setUserFirstName(newUser.getUserFirstName());
        oldUser.setUserLastName(newUser.getUserLastName());
        oldUser.setUserPassword(newUser.getUserPassword());
        oldUser.setUserZIPCode(newUser.getUserZIPCode());
        oldUser.setUserPhoneNumber(newUser.getUserPhoneNumber());
        update(oldUser);
    }

    public boolean isExists(String expression, GenObject<?> criterion) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.where(builder.equal(root.get(expression), criterion.getInstance()));
        return !entityManager.createQuery(criteria).getResultList().isEmpty();
    }

}
