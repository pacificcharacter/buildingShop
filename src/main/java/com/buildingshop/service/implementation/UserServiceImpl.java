package com.buildingshop.service.implementation;


import com.buildingshop.dao.interfaces.DAO;
import com.buildingshop.domains.User;
import com.buildingshop.service.interfaces.UserService;
import com.buildingshop.utils.GenObject;
import org.springframework.security.access.annotation.Secured;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Stateless
@Local(UserService.class)
public class UserServiceImpl implements UserService {

    @Inject
    @Named("userDAO")
    private DAO<User> userDAO;

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_SUPER_ADMIN"})
    public List<User> getAllUsers() {
        return userDAO.getAll();
    }

    public void registerUser(User user) {
        userDAO.add(user);
    }

    public User getUser(String expression, GenObject<?> criterion) {
        return userDAO.get(expression, criterion);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_SUPER_ADMIN"})
    public void deleteUser(Long userID) {
        userDAO.delete(userID);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_SUPER_ADMIN"})
    public void deleteUser(String expression, GenObject<?> criterion) {
        userDAO.delete(expression, criterion);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_SUPER_ADMIN"})
    public List<User> getLike(String expression, GenObject<?> criterion) {
        return userDAO.getLike(expression, criterion);
    }

    public void updateUser(User user) {
        userDAO.update(user);
    }

    public void updateUser(User oldUser, User newUser) {
        userDAO.update(oldUser, newUser);
    }

    public boolean isExists(String expression, GenObject<?> criterion) {
        return (userDAO.isExists(expression, criterion));
    }

}
