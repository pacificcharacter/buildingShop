package com.buildingshop.service.interfaces;

import com.buildingshop.domains.User;
import com.buildingshop.utils.GenObject;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    void registerUser(User user);

    User getUser(String expression, GenObject<?> criterion);

    void deleteUser(Long userID);

    void deleteUser(String expression, GenObject<?> criterion);

    List<User> getLike(String expression, GenObject<?> criterion);

    void updateUser(User user);

    void updateUser(User oldUser, User newUser);

    boolean isExists(String expression, GenObject<?> criterion);

}
