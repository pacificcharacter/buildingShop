function changeRole(userID) {
    var errorCount = 0;
    if($('#roleUpd').hasClass('btn-success')) {
        var e = document.getElementById("roles");
        var role = e.options[e.selectedIndex].text;

        $.ajax({
            type: "POST",
            url: "/user/admin/setRole",
            data: "role=" + role + "&userID=" + userID,
            dataType: "json",
            async: false,
            success: function(data) {
                if (data) {
                    if (!$(".alert").length) {
                        $("<div class='alert alert-success'><strong>Role has successfully changed.</strong></div>").insertBefore($(".panel"));
                        var alert = $(".alert");
                        alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                        $("#close").click(function () {
                            $(".alert").fadeOut(300, function () {
                                $(this).remove();
                            });
                        })
                    }
                }
                else {
                    if (!$(".alert").length) {
                        $("<div class='alert alert-warning'><strong>This user already has the status.</strong></div>").insertBefore($(".panel"));
                        var alert = $(".alert");
                        alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                        $("#close").click(function () {
                            $(".alert").fadeOut(300, function () {
                                $(this).remove();
                            });
                        })
                    }
                }
            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
            }
        });
    }
    else {

        $("#roleUpd").removeClass("btn-warning");
        $("#roleUpd").addClass("btn-success");
        var currentRole = $("#mainRole").text();

        $(document).find('.selector').each(function() {
            var select = $('<select id="roles" class="form-control" />');
            $(this).replaceWith(select);
        });

        setSelectedRole(currentRole);
    }

}

function setSelectedRole(role) {

    var roles = ["ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"];
    if(sessionRole==="ROLE_ADMIN") {
        roles.splice(0, 1);
    }

    var select = document.getElementById("roles");

    roles.forEach(function(i) {
        var el = document.createElement("option");
        el.textContent = i;
        el.value = i;

        if(i === role) el.selected = true;

        select.appendChild(el);
    });
}