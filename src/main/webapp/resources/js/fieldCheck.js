var errorCount = 0;

function checkFirstName() {

    var firstName = $("#firstName");
    var nameInput = $("#4").val();
    var status = false;

    $.ajax({
        type: "POST",
        url: "/user/firstNameValidation",
        data: "firstName=" + nameInput,
        dataType: "json",
        async: false,

        success: function (data) {

            if (data.isValid) {
                firstName.addClass("has-success has-feedback");
                firstName.addClass("glyphicon-ok");
                firstName.addClass("control-label").html("Name is valid");

               status = true;
            }
            else {
                firstName.addClass("has-error has-feedback");
                firstName.addClass("glyphicon-remove");
                firstName.addClass("control-label").html("Name "+data.name);

                status = false;
            }
        },
        error: function() {
            errorCount++;
            if(!$(".alert").length) {
                $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                var alert = $(".alert");
                alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                $("#close").click(function () {
                    $(".alert").fadeOut(300, function () {
                        $(this).remove();
                    });
                })
            }
            else {
                if (!$(".badge").length) {
                    var counter = $("<span class='badge'></span>").html(errorCount);
                    counter.insertBefore($(".close"));
                }
                else $(".badge").html(errorCount);
            }
            return errorCount;
        }
    });
    return status;
}

function checkLastName() {

    var lastNameInput = $("#5").val();
    var lastName = $("#lastName");
    var status = false;

    $.ajax({
        type: "POST",
        url: "/user/lastNameValidation",
        data: "lastName=" + lastNameInput,
        dataType: "json",
        async: false,

        success: function (data) {

            if (data.isValid) {
                lastName.addClass("has-success has-feedback");
                lastName.addClass("glyphicon-ok");
                lastName.addClass("control-label").html("Last name is valid");

                status = true;
            }
            else {
                lastName.addClass("has-error has-feedback");
                lastName.addClass("glyphicon-remove");
                lastName.addClass("control-label").html("Surname "+data.lastname);

                status = false;
            }
        },
        error: function() {
            errorCount++;
            if(!$(".alert").length) {
                $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                var alert = $(".alert");
                alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                $("#close").click(function () {
                    $(".alert").fadeOut(300, function () {
                        $(this).remove();
                    });
                })
            }
            else {
                if (!$(".badge").length) {
                    var counter = $("<span class='badge'></span>").html(errorCount);
                    counter.insertBefore($(".close"));
                }
                else $(".badge").html(errorCount);
            }
            status = false;
        }
    });
    return status;
}

function checkZipCode() {

    var zip = $("#zip");
    var zipInput = $("#6").val();
    var status = false;

    $.ajax({
        type: "POST",
        url: "/user/zipCodeValidation",
        data: "zipCode=" + zipInput,
        dataType: "json",
        async: false,

        success: function (data) {

            if (data.isValid) {
                zip.addClass("has-success has-feedback");
                zip.addClass("glyphicon-ok");
                zip.addClass("control-label").html("ZIP-code is valid");

                status = true;
            }
            else {
                zip.addClass("has-error has-feedback");
                zip.addClass("glyphicon-remove");
                zip.addClass("control-label").html("ZIP-code " + data.zip);

                status = false;
            }
        },
        error: function() {
            errorCount++;
            if(!$(".alert").length) {
                $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                var alert = $(".alert");
                alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                $("#close").click(function () {
                    $(".alert").fadeOut(300, function () {
                        $(this).remove();
                    });
                })
            }
            else {
                if (!$(".badge").length) {
                    var counter = $("<span class='badge'></span>").html(errorCount);
                    counter.insertBefore($(".close"));
                }
                else $(".badge").html(errorCount);
            }
            status = false;
        }
    });
    return status;
}