function checkOldPassword() {

    var passwordGroup = $("#8.form-group");
    var passwordSpan = $("#OldPasswordSpan");
    var passwordLabel = $("#OldPasswordLabel");
    var hasSuccess = $("#2.has-success");
    var hasError = $("#2.has-error");
    var passwordInput = $("#OldPassword").val();
    var status = false;

    $.ajax({
        type: "POST",
        url: "/user/oldPasswordValidation",
        data: "password=" + passwordInput + "&email=" + $("#1").text(),
        dataType: "json",
        async: false,
        success: function(data) {
            if(data.isValid) {
                if (hasError.length) {
                    passwordGroup.removeClass("has-error has-feedback");
                    passwordSpan.removeClass("glyphicon-remove");
                }
                passwordGroup.addClass("has-success has-feedback");
                passwordSpan.addClass("glyphicon-ok");
                passwordLabel.addClass("control-label").html("Password matches");

                status= true;
            }
            else {
                if (hasSuccess.length) {
                    passwordGroup.removeClass("has-success has-feedback");
                    passwordSpan.removeClass("glyphicon-ok");
                }
                passwordGroup.addClass("has-error has-feedback");
                passwordSpan.addClass("glyphicon-remove");
                passwordLabel.addClass("control-label").html("Password not matches");

                status= false;
            }
        },
        error: function() {
            errorCount++;
            if(!$(".alert").length) {
                $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                var alert = $(".alert");
                alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                $("#close").click(function () {
                    $(".alert").fadeOut(300, function () {
                        $(this).remove();
                    });
                })
            }
            else {
                if (!$(".badge").length) {
                    var counter = $("<span class='badge'></span>").html(errorCount);
                    counter.insertBefore($(".close"));
                }
                else $(".badge").html(errorCount);
            }
            status= false;
        }
    });
    return status;
}

function checkNewPassword() {

    var passwordGroup = $("#2.form-group");
    var passwordSpan = $("#NewPasswordSpan");
    var passwordLabel = $("#NewPasswordLabel");
    var hasSuccess = $("#2.has-success");
    var hasError = $("#2.has-error");
    var passwordInput = $("#NewPassword").val();
    var confirmInput = $("#Confirm");
    var confirmSpan = $("#ConfirmSpan");
    var confirmLabel = $("#ConfirmLabel");
    var confirmGroup = $("#3.form-group");
    var confirmHasError = $("#3.has-error");
    var status = false;

    $.ajax({
        type: "POST",
        url: "/user/passwordValidation",
        data: "password=" + passwordInput,
        dataType: "json",
        async: false,
        success: function(data) {
            if(data.isValid) {
                if (hasError.length) {
                    passwordGroup.removeClass("has-error has-feedback");
                    passwordSpan.removeClass("glyphicon-remove");
                }
                passwordGroup.addClass("has-success has-feedback");
                passwordSpan.addClass("glyphicon-ok");
                passwordLabel.addClass("control-label").html("Password is valid");



                if (passwordInput === confirmInput.val() && passwordInput !== "") {
                    if (confirmHasError.length) {
                        confirmGroup.removeClass("has-error has-feedback");
                        confirmLabel.removeClass("control-label");
                        confirmSpan.removeClass("glyphicon-remove");
                    }
                    confirmGroup.addClass("has-success has-feedback");
                    confirmSpan.removeClass("glyphicon-remove");
                    confirmSpan.addClass("glyphicon-ok");
                    confirmLabel.addClass("control-label").html("Confirmed");
                }
                else {
                    if (confirmInput.val() !== "") {
                        if (hasSuccess.length) {
                            confirmGroup.removeClass("has-success has-feedback");
                            confirmLabel.removeClass("control-label");
                            confirmSpan.removeClass("glyphicon-ok");
                        }
                        confirmGroup.addClass("has-error has-feedback");
                        confirmSpan.addClass("glyphicon-remove");
                        confirmLabel.addClass("control-label").html("Check password");
                    }
                }
                status= true;
            }
            else {
                if (hasSuccess.length) {
                    passwordGroup.removeClass("has-success has-feedback");
                    passwordSpan.removeClass("glyphicon-ok");
                }
                passwordGroup.addClass("has-error has-feedback");
                passwordSpan.addClass("glyphicon-remove");
                passwordLabel.addClass("control-label").html("Password" + data.password);

                status= false;
            }
        },
        error: function() {
            errorCount++;
            if(!$(".alert").length) {
                $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                var alert = $(".alert");
                alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                $("#close").click(function () {
                    $(".alert").fadeOut(300, function () {
                        $(this).remove();
                    });
                })
            }
            else {
                if (!$(".badge").length) {
                    var counter = $("<span class='badge'></span>").html(errorCount);
                    counter.insertBefore($(".close"));
                }
                else $(".badge").html(errorCount);
            }
            status= false;
        }
    });
    return status;
}

function confirmNewPassword() {

    var passwordInput = $("#NewPassword");
    var confirmInput = $("#Confirm");
    var hasError = $("#3.has-error");
    var confirmGroup = $("#3.form-group");
    var confirmLabel = $("#ConfirmLabel");
    var confirmSpan = $("#ConfirmSpan");
    var passwordSpan = $("#PasswordSpan");
    var passwordLabel = $("#PasswordLabel");

    if (passwordInput.val() === confirmInput.val() && confirmInput.val() !== "") {

        if (hasError.length) {
            confirmGroup.removeClass("has-error has-feedback");
            confirmLabel.removeClass("control-label");
            confirmSpan.removeClass("glyphicon-remove");
        }
        confirmGroup.addClass("has-success has-feedback");
        confirmSpan.addClass("glyphicon-ok");
        confirmLabel.addClass("control-label").html("Password confirmed");

        passwordSpan.removeClass("glyphicon-remove");
        passwordSpan.addClass("glyphicon-ok");
        passwordLabel.addClass("control-label");

        return true;
    }
    else {
        confirmGroup.removeClass("has-success");
        confirmLabel.removeClass("control-label");
        confirmGroup.addClass("has-error has-feedback");
        confirmSpan.addClass("glyphicon-remove");
        confirmLabel.addClass("control-label").html("Check password");

        return false;
    }
}