var sample = {
    namespace : {
    }
};

sample.namespace.Roles = {
  'ROLE_USER': 0,
  'ROLE_MODERATOR': 1,
  'ROLE_ADMIN': 2,
  'ROLE_SUPER_ADMIN' : 3
};

var roles = new Array(3);
var count = 0;
var sessionRole;

function addRole(role) {
    roles[count] = role;
    count++;
}

function setSessionRole(sRole) {
    sessionRole = sRole;
}

function mainRole() {
    var mr = 0,
        mRole = 'ROLE_USER',
        divRole = document.getElementById("mainRole");

    var obj = sample.namespace.Roles;

    roles.forEach(function(i) {
        if(obj[i]>mr) {
            mr = obj[i];
            mRole = i;
        }
    });
    divRole.innerHTML = mRole;
}