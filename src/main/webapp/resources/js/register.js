$(document).ready(function(){

    var checkArray = new Array(6);
    var errorCount = 0;

    function enabler() {
        var check = 0;
        for (var i = 0; i < checkArray.length; i++) {
            if (checkArray[i] === true) {
                check++;
            }
        }
        return check;
    }

    $("#1").focusout(function() {

        var emailGroup = $("#1.form-group");
        var emailSpan = $("#MailSpan");
        var emailLabel = $("#EmailLabel");
        var hasError = $("#1.has-error");
        var hasSuccess = $("#1.has-success");
        var emailInput = $("#Email").val();
        
        $.ajax({
            type: "POST",
            url: "/user/emailValidation",
            data: "email=" + emailInput,
            dataType: "json",

            success: function (data) {
                if (data.isValid ) {
                    if (hasError.length) {
                        emailGroup.removeClass("has-error has-feedback");
                        emailSpan.removeClass("glyphicon-remove");
                    }
                    emailGroup.addClass("has-success has-feedback");
                    emailSpan.addClass("glyphicon-ok");
                    emailLabel.addClass("control-label").html("Email is valid");

                    checkArray[0] = true;
                    if (enabler() === 7) {
                        $("#submitThis").prop("disabled", false);
                    }
                }
                else {
                    if (hasSuccess.length) {
                        emailGroup.removeClass("has-success has-feedback");
                        emailSpan.removeClass("glyphicon-ok");
                    }
                    emailGroup.addClass("has-error has-feedback");
                    emailSpan.addClass("glyphicon-remove");
                    emailLabel.addClass("control-label").html("Email" + data.email);

                    checkArray[0] = false;
                    if (enabler() !== 7) {
                        $("#submitThis").prop("disabled", true);
                    }
                }
            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
                return errorCount;
            }
        });
        return false;
    });

    $("#2").focusout(function() {

        var passwordGroup = $("#2.form-group");
        var passwordSpan = $("#PasswordSpan");
        var passwordLabel = $("#PasswordLabel");
        var hasSuccess = $("#2.has-success");
        var hasError = $("#2.has-error");
        var passwordInput = $("#Password").val();
        var confirmInput = $("#Confirm");
        var confirmSpan = $("#ConfirmSpan");
        var confirmLabel = $("#ConfirmLabel");
        var confirmGroup = $("#3.form-group");
        var confirmHasError = $("#3.has-error");

        $.ajax({
            type: "POST",
            url: "/user/passwordValidation",
            data: "password=" + passwordInput,
            dataType: "json",

            success: function(data) {

                if(data.isValid) {

                    if (hasError.length) {
                        passwordGroup.removeClass("has-error has-feedback");
                        passwordSpan.removeClass("glyphicon-remove");
                    }
                    passwordGroup.addClass("has-success has-feedback");
                    passwordSpan.addClass("glyphicon-ok");
                    passwordLabel.addClass("control-label").html("Password is valid");

                    checkArray[1] = true;
                    if (enabler() === 7) {
                        $("#submitThis").prop("disabled", false);
                    }

                    if (passwordInput.val() === confirmInput.val() && passwordInput.val() !== "") {
                        if (confirmHasError.length) {
                            confirmGroup.removeClass("has-error has-feedback");
                            confirmLabel.removeClass("control-label");
                            confirmSpan.removeClass("glyphicon-remove");
                        }
                        confirmGroup.addClass("has-success has-feedback");
                        confirmSpan.removeClass("glyphicon-remove");
                        confirmSpan.addClass("glyphicon-ok");
                        confirmLabel.addClass("control-label").html("Confirmed");
                    }
                    else {
                        if (confirmInput.val() !== "") {
                            if (hasSuccess.length) {
                                confirmGroup.removeClass("has-success has-feedback");
                                confirmLabel.removeClass("control-label");
                                confirmSpan.removeClass("glyphicon-ok");
                            }
                            confirmGroup.addClass("has-error has-feedback");
                            confirmSpan.addClass("glyphicon-remove");
                            confirmLabel.addClass("control-label").html("Check password");
                        }
                    }
                }
                else {
                    if (hasSuccess.length) {
                        passwordGroup.removeClass("has-success has-feedback");
                        passwordSpan.removeClass("glyphicon-ok");
                    }
                    passwordGroup.addClass("has-error has-feedback");
                    passwordSpan.addClass("glyphicon-remove");
                    passwordLabel.addClass("control-label").html("Password" + data.password);

                    checkArray[1] = false;
                    if (enabler() !== 7) {
                        $("#submitThis").prop("disabled", true);
                    }
                }
            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
                return errorCount;
            }
        });
        return false;
    });

    $("#3").focusout(function() {

        var passwordInput = $("#Password");
        var confirmInput = $("#Confirm");
        var hasError = $("#3.has-error");
        var confirmGroup = $("#3.form-group");
        var confirmLabel = $("#ConfirmLabel");
        var confirmSpan = $("#ConfirmSpan");
        var passwordSpan = $("#PasswordSpan");
        var passwordLabel = $("#PasswordLabel");

        if (passwordInput.val() === confirmInput.val() && confirmInput.val() !== "") {

            if (hasError.length) {
                confirmGroup.removeClass("has-error has-feedback");
                confirmLabel.removeClass("control-label");
                confirmSpan.removeClass("glyphicon-remove");
            }
            confirmGroup.addClass("has-success has-feedback");
            confirmSpan.addClass("glyphicon-ok");
            confirmLabel.addClass("control-label").html("Password confirmed");

            passwordSpan.removeClass("glyphicon-remove");
            passwordSpan.addClass("glyphicon-ok");
            passwordLabel.addClass("control-label");

            checkArray[2] = true;
            if (enabler() === 7) {
                $("#submitThis").prop("disabled", false);
            }
        }
        else {
            confirmGroup.removeClass("has-success");
            confirmLabel.removeClass("control-label");
            confirmGroup.addClass("has-error has-feedback");
            confirmSpan.addClass("glyphicon-remove");
            confirmLabel.addClass("control-label").html("Check password");

            checkArray[2] = false;
            if (enabler() !== 7) {
                $("#submitThis").prop("disabled", true);
            }
        }
    });

    $("#4").focusout(function() {

        var nameGroup = $("#4.form-group");
        var hasError = $("#4.has-error");
        var nameLabel = $("#NameLabel");
        var nameSpan = $("#NameSpan");
        var hasSuccess = $("#4.has-success");
        var nameInput = $("#Name").val();

        $.ajax({
            type: "POST",
            url: "/user/firstNameValidation",
            data: "firstName=" + nameInput,
            dataType: "json",

            success: function (data) {

                if (data.isValid) {
                    if (hasError.length) {
                        nameGroup.removeClass("has-error has-feedback");
                        nameLabel.removeClass("control-label");
                        nameSpan.removeClass("glyphicon-remove");
                    }
                    nameGroup.addClass("has-success has-feedback");
                    nameSpan.addClass("glyphicon-ok");
                    nameLabel.addClass("control-label").html("Name is valid");

                    checkArray[3] = true;
                    if (enabler() === 7) {
                        $("#submitThis").prop("disabled", false);
                    }
                }
                else {
                    if (hasSuccess.length) {
                        nameGroup.removeClass("has-success has-feedback");
                        nameSpan.removeClass("glyphicon-ok");
                    }
                    nameGroup.addClass("has-error has-feedback");
                    nameSpan.addClass("glyphicon-remove");
                    nameLabel.addClass("control-label").html(data.name);

                    checkArray[3] = false;
                    if (enabler() !== 7){
                        $("#submitThis").prop("disabled", true);
                    }
                }
            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
                return errorCount;
            }
        });
        return false;
    });

    $("#5").focusout(function() {

        var lastNameInput = $("#Lastname").val();
        var lastNameGroup = $("#5.form-group");
        var hasError = $("#5.has-error");
        var lastNameSpan = $("#LastnameSpan");
        var lastNameLabel = $("#LastnameLabel");
        var hasSuccess = $("#5.has-success");

        $.ajax({
            type: "POST",
            url: "/user/lastNameValidation",
            data: "lastName=" + lastNameInput,
            dataType: "json",

            success: function (data) {

                if (data.isValid) {
                    if(hasError.length) {
                        lastNameGroup.removeClass("has-error has-feedback");
                        lastNameSpan.removeClass("glyphicon-remove");
                    }
                    lastNameGroup.addClass("has-success has-feedback");
                    lastNameSpan.addClass("glyphicon-ok");
                    lastNameLabel.addClass("control-label").html("Last name is valid");

                    checkArray[4] = true;
                    if (enabler() === 7) {
                        $("#submitThis").prop("disabled", false);
                    }
                }
                else {
                    if(hasSuccess.length) {
                        hasError.removeClass("has-success has-feedback");
                        lastNameSpan.removeClass("glyphicon-ok");
                    }
                    lastNameGroup.addClass("has-error has-feedback");
                    lastNameSpan.addClass("glyphicon-remove");
                    lastNameLabel.addClass("control-label").html(data.lastname);

                    checkArray[4] = false;
                    if (enabler() !== 7) {
                        $("#submitThis").prop("disabled", true);
                    }
                }
            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
                return errorCount;
            }
        });
        return false;
    });

    $("#6").focusout(function() {

        var zipGroup = $("#6.form-group");
        var hasError = $("#6.has-error");
        var hasSuccess = $("#6.has-success");
        var zipSpan = $("#ZIPSpan");
        var zipLabel = $("#ZIPLabel");
        var zipInput = $("#ZIP").val();

        $.ajax({
            type: "POST",
            url: "/user/zipCodeValidation",
            data: "zipCode=" + zipInput,
            dataType: "json",

            success: function (data) {

                if (data.isValid) {
                    if(hasError.length) {
                        zipGroup.removeClass("has-error has-feedback");
                        zipSpan.removeClass("glyphicon-remove");
                    }
                    zipGroup.addClass("has-success has-feedback");
                    zipSpan.addClass("glyphicon-ok");
                    zipLabel.addClass("control-label").html("ZIP-code is valid");

                    checkArray[5] = true;
                    if (enabler() === 7) {
                        $("#submitThis").prop("disabled", false);
                    }
                }
                else {
                    if(hasSuccess.length) {
                        zipGroup.removeClass("has-success has-feedback");
                        zipSpan.removeClass("glyphicon-ok");
                    }
                    zipGroup.addClass("has-error has-feedback");
                    zipSpan.addClass("glyphicon-remove");
                    zipLabel.addClass("control-label").html(data.zip);

                    checkArray[5] = false;
                    if (enabler() !== 7) {
                        $("#submitThis").prop("disabled", true);
                    }
                }
            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
                return errorCount;
            }
        });
        return false;
    });

    $("#7").focusout(function() {

        var phoneGroup = $("#7.form-group");
        var hasError = $("#7.has-error");
        var hasSuccess = $("#7.has-success");
        var phoneSpan = $("#PhoneSpan");
        var phoneLabel =  $("#PhoneLabel");
        var phoneInput = $("#Phone").val();

        $.ajax({
            type: "POST",
            url: "/user/phoneNumberValidation",
            data: "phoneNumber=" + phoneInput,
            dataType: "json",

            success: function (data) {

                if (data.isValid) {
                    if(hasError.length) {
                        phoneGroup.removeClass("has-error has-feedback");
                        phoneSpan.removeClass("glyphicon-remove");
                    }
                    phoneGroup.addClass("has-success has-feedback");
                    phoneSpan.addClass("glyphicon-ok");
                    phoneLabel.addClass("control-label").html("Phone number is valid");

                    checkArray[6] = true;
                    if (enabler() === 7) {
                        $("#submitThis").prop("disabled", false);
                    }
                }
                else {
                    if(hasSuccess.length) {
                        phoneGroup.removeClass("has-success has-feedback");
                        phoneSpan.removeClass("glyphicon-remove");
                    }
                    phoneGroup.addClass("has-error has-feedback");
                    phoneSpan.addClass("glyphicon-remove");
                    phoneLabel.addClass("control-label").html(data.phone);

                    checkArray[6] = false;
                    console.log(enabler());
                    if (enabler() !== 7) {
                        $("#submitThis").prop("disabled", true);
                    }
                }
            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
                return errorCount;
            }
        });
        return false;
    });

    // $("#submitThis").click(function () {
    //
    //     if (enabler() === 7) {
    //         var email = $("#Email").val();
    //         var password = $("#Password").val();
    //         var name = $("#Name").val();
    //         var lastName = $("#Lastname").val();
    //         var zip = $("#ZIP").val();
    //         var phone = $("#Phone").val();
    //
    //         $.ajax({
    //             type: "POST",
    //             url: "registration",
    //             data: JSON.stringify({
    //                 email: email,
    //                 password: password,
    //                 name: name,
    //                 lastName: lastName,
    //                 zip: zip,
    //                 phone: phone
    //             }),
    //             dataType: "json",
    //
    //             success: function(data) {
    //                 if (data.isValid) {
    //                    
    //                     var welcome = "Welcome " + data.name + " " + data.lastName + "!";
    //                    
    //                     $("<div class='alert alert-success'><strong>Successful! </strong></div>").insertBefore($("#auth"));
    //                     var alert = $(".alert");
    //                     alert.append($("<span class='welcome'>sdf</span>").text(welcome));
    //                     alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
    //                     setTimeout(function () {
    //                         $("#auth").remove();
    //                     }, 1000);
    //                     $("#close").click(function () {
    //                         $(".alert").fadeOut(300, function () {
    //                             $(this).remove();
    //                         });
    //                     });
    //                     setTimeout(function () {
    //                         $(".alert").fadeOut(500, function () {
    //                             $(this).remove();
    //                         });
    //                     }, 7000);
    //                 }
    //             },
    //             error: function () {
    //                 if(!$(".alert").length) {
    //                     $("<div class='alert alert-danger'><strong>Oh... </strong><span>Some problems on server.</span>" +
    //                         "<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>" +
    //                         "</div>").insertBefore($("#auth"));
    //                     $("#close").click(function () {
    //                         $(".alert").fadeOut(300, function () {
    //                             $(this).remove();
    //                         });
    //                     });
    //                 }
    //             }
    //         });
    //     }
    //     else {
    //         if(!$(".alert").length) {
    //             $("<div class='alert alert-danger'><strong>Stop-stop-stop </strong><span>It will not work.</span>" +
    //                 "<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>" +
    //                 "</div>").insertBefore($("#auth"));
    //             $("#close").click(function () {
    //                 $(".alert").fadeOut(300, function () {
    //                     $(this).remove();
    //                 });
    //                 setTimeout(function () {
    //                     $(".alert").fadeOut(500, function () {
    //                         $(".alert").remove();
    //                     });
    //                 }, 7000);
    //             });
    //         }
    //     }
    // });
});