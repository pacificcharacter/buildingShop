var req;
var isIE;
var completeTable, tbodyComplete;

function init() {
    completeTable = document.getElementById("complete-table");
}

function doCompletion() {

    var wdiv = document.createElement("div"),
        wspan = document.createElement("span");

    if(document.getElementById("search_concept").innerHTML.toLowerCase() == "filter by") {
        wdiv.className = "alert alert-danger";
        wdiv.id = "empty-filter";
        wspan.innerHTML = "You must choose filter before search!";
        wdiv.appendChild(wspan);
        if(!document.contains(document.getElementById("empty-filter"))) document.getElementById("search-line").appendChild(wdiv);
    }

    else {
        if((2 > document.getElementById("complete-field").value.length)&&(document.getElementById("search_concept").innerHTML.toLowerCase() != "anything")) return;
        if (document.contains(document.getElementById("empty-filter"))) document.getElementById("empty-filter").remove();
        getUsers();
    }
}

function doDelete(i) {

    req = initRequest();
    req.open("POST", '/user/search/delete', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send("&userID=" + i);
    req.onreadystatechange = function() {
      if (req.readyState == 4) {
        if(req.status == 200) {
          console.log("delete complete");
        }
      }
    };
}

function initRequest() {
    if (window.XMLHttpRequest) {
        if (navigator.userAgent.indexOf('MSIE') != -1) {
            isIE = true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

function deleteRow(id, i){
    doDelete(id);
    document.getElementById('complete-table').deleteRow(i);
}

function appendUser(firstName, lastName, userId, email, phoneNumber, zipCode) {

    var trd, tdName, aFLName, tdEmail, tdPhone, tdZip, tdButton, deleteButton;

    trd = document.createElement("tr");

    tdName = document.createElement("td");
    tdName.id = "userFirstLastName";
    tdName.name = "userFirstLastName";

    aFLName = document.createElement('a');
    aFLName.href =  'account/' + userId;
    aFLName.innerHTML = firstName + " " + lastName;

    tdName.appendChild(aFLName);

    tdEmail = document.createElement("td");
    tdEmail.id = "userEmail";
    tdEmail.name = "userEmail";
    tdEmail.appendChild(document.createTextNode(email));

    tdPhone = document.createElement("td");
    tdPhone.id = "userPhoneNumber";
    tdPhone.name = "userPhoneNumber";
    tdPhone.appendChild(document.createTextNode(phoneNumber));

    tdZip = document.createElement("td");
    tdZip.id = "userZIPCode";
    tdZip.name = "userZIPCode";
    tdZip.appendChild(document.createTextNode(zipCode));

    tdButton = document.createElement("td");
    tdButton.id = "user-item";
    tdButton.name = "user-item";


    deleteButton = document.createElement('button');
    deleteButton.className = "btn btn-danger";
    var t = document.createTextNode("Delete");
    deleteButton.appendChild(t);
    deleteButton.id = userId;
    deleteButton.onclick = function() {deleteRow(this.getAttribute('id'), this.parentNode.parentNode.rowIndex)};

    tdButton.appendChild(deleteButton);

    trd.appendChild(tdName);
    trd.appendChild(tdEmail);
    trd.appendChild(tdPhone);
    trd.appendChild(tdZip);
    trd.appendChild(tdButton);

    tbodyComplete.appendChild(trd);

}

function clearTable() {
    if (completeTable.getElementsByTagName("tr").length > 0) {
        for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
            completeTable.removeChild(completeTable.childNodes[loop]);
        }
    }

    var trh, thName, thEmail, thPhone, thZip, thDelete, thead;
    thead = document.createElement("thead");
    trh = document.createElement("tr");

    thName = document.createElement("th");
    thName.appendChild(document.createTextNode("Name"));
    trh.appendChild(thName);

    thEmail = document.createElement("th");
    thEmail.appendChild(document.createTextNode("E-mail"));
    trh.appendChild(thEmail);

    thPhone = document.createElement("th");
    thPhone.appendChild(document.createTextNode("Phone number"));
    trh.appendChild(thPhone);

    thZip = document.createElement("th");
    thZip.appendChild(document.createTextNode("ZIP-code"));
    trh.appendChild(thZip);

    thDelete = document.createElement("th");
    thDelete.appendChild(document.createTextNode("Delete"));
    trh.appendChild(thDelete);

    thead.appendChild(trh);

    completeTable.appendChild(thead);

}

function getUsers() {

    $.ajax({
        type: "GET",
        url: "/user/search/getUser",
        data: "expression=" + escape(document.getElementById("search_concept").innerHTML.toLowerCase()) + "&criterion="+ escape(document.getElementById("complete-field").value),
        dataType: "json",
        //async: false,
        success: function (data) {
            clearTable();
            tbodyComplete = completeTable.appendChild(document.createElement("tbody"));
            for (var i in data.result) {
                 appendUser(data.result[i].userFirstName,
                 data.result[i].userLastName,
                 data.result[i].userID,
                 data.result[i].userEmail,
                 data.result[i].userPhoneNumber,
                 data.result[i].userZIPCode);
            }

        },
        error: function() {
            clearTable();
            var row = document.createElement("tr");

            var item = document.createElement("td");
            item.className = "alert alert-info";
            item.name = "user-item";

            var header = document.createElement("h4");
            header.className = "list-group-item-heading";
            header.id = "error";
            header.name = "error";
            header.appendChild(document.createTextNode("0 matches!"));

            item.appendChild(header);

            row.appendChild(item);

            completeTable.appendChild(row);
        }
    });
}