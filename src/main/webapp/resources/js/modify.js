function changePassword() {
    var errorCount = 0;
    if(checkNewPassword()&&confirmNewPassword()&&checkOldPassword()) {

        $.ajax({
            type: "POST",
            url: "/user/changePassword",
            data: "password=" + $("#NewPassword").val() + "&email=" + $("#1").text(),
            dataType: "json",
            async: false,
            success: function(data) {

                $("#PasswordForm").remove();
                $('<div class="col-md-4 col-md-offset-4"><div class="panel panel-info"><div class="panel-heading">'+
                    '<h3 class="panel-title">Password was successfully changed!</h3></div></div></div>').insertBefore("#main");

            },
            error: function() {
                errorCount++;
                if(!$(".alert").length) {
                    $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                    var alert = $(".alert");
                    alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                    $("#close").click(function () {
                        $(".alert").fadeOut(300, function () {
                            $(this).remove();
                        });
                    })
                }
                else {
                    if (!$(".badge").length) {
                        var counter = $("<span class='badge'></span>").html(errorCount);
                        counter.insertBefore($(".close"));
                    }
                    else $(".badge").html(errorCount);
                }
            }
        });

    }
}
function modify() {

    if($('#preUpd').hasClass('btn-success')) {
        if(checkFirstName()&&checkLastName()&&checkZipCode()) {

            $.ajax({
                type: "POST",
                url: "/user/update",
                data: "name=" + $("#4").val() + "&email=" + $("#1").text() + "&lastName=" + $("#5").val() + "&zip=" + $("#6").val(),
                dataType: "json",
                async: false,
                success: function(data) {

                    $("#update").remove();
                    $('<div class="col-md-4 col-md-offset-4"><div class="panel panel-info"><div class="panel-heading">'+
                           '<h3 class="panel-title">Profile information was successfully updated!</h3><a href="/user/account" class="btn btn-sm btn-default form-control">Back to account</a></div></div></div>').insertBefore("#main");

                },
                error: function() {
                    errorCount++;
                    if(!$(".alert").length) {
                        $("<div class='alert alert-danger'><strong>Error data transfer.</strong><span> Server not response. </span></div>").insertBefore($(".panel"));
                        var alert = $(".alert");
                        alert.append($("<button type='button' class='close' data-dismiss='alert' id='close' aria-label='Close'><span aria-hidden='false'>&times;</span></button>"));
                        $("#close").click(function () {
                            $(".alert").fadeOut(300, function () {
                                $(this).remove();
                            });
                        })
                    }
                    else {
                        if (!$(".badge").length) {
                            var counter = $("<span class='badge'></span>").html(errorCount);
                            counter.insertBefore($(".close"));
                        }
                        else $(".badge").html(errorCount);
                    }
                }
            });
        }

    }
    else {
        $("#preUpd").removeClass("btn-warning");
        $("#preUpd").addClass("btn-success");
        var j = 4;
        $(document).find('.inp').each(function() {
            var input = $('<input id="'+j+'" class="form-control input-sm" />').val($(this).text());
            $(this).replaceWith(input);
            j++;
        });

        $("#main").append('<div class="col-md-4 col-md-offset-4" id="PasswordForm"><div class="panel panel-info"><div class="panel-heading">'+
            '<h3 class="panel-title">Wanna change password?</h3></div><div class="panel-body">'+
            '<div id="upd-password"><div class="form-group" id="8">'+
                '<label  for="OldPassword" id="OldPasswordLabel">Old Password</label>'+
                '<input path="userPassword" name="password" type="password" class="form-control input-sm" id="OldPassword" placeholder="Enter old password"/>'+
                '<span class="glyphicon form-control-feedback" id="OldPasswordSpan"></span>'+
            '</div><div class="form-group" id="2">'+
                '<label  for="NewPassword" id="NewPasswordLabel">New Password</label>'+
                '<input path="userPassword" name="new-password" type="password" class="form-control input-sm" id="NewPassword" placeholder="Enter new password"/>'+
                '<span class="glyphicon form-control-feedback" id="PasswordSpan"></span>'+
            '</div>'+
            '<div class="form-group" id="3">'+
            '<label  for="Confirm" id="ConfirmLabel">Confirm password</label>'+
            '<input name="confirm" type="password" class="form-control input-sm" id="Confirm" placeholder="Enter new password again">'+
            '<span class="glyphicon form-control-feedback" id="ConfirmSpan"></span>'+
            '</div><button type="submit" class="btn btn-sm btn-success" id="updatePassword" onclick="changePassword()">Change password</button></div></div></div></div>');

    }
}