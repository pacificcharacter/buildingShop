<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Account</title>
    <meta charset="utf-8">
    <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jqueryURL"/>
    <spring:url value="/resources/js/modify.js" var="modifyURL"/>
    <spring:url value="/resources/js/fieldCheck.js" var="fieldsCheckURL"/>
    <spring:url value="/resources/js/rolesEdit.js" var="rolesURL"/>
    <spring:url value="/resources/js/roles.js" var="roleURL"/>
    <spring:url value="/resources/js/checkPasswords.js" var="checkPasswordsURL"/>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="cssURL"/>
    <link href="${cssURL}" rel="stylesheet"/>
    <script src="${fieldsCheckURL}"></script>
    <script src="${checkPasswordsURL}"></script>
    <script src="${jqueryURL}"></script>
    <script src="${roleURL}"></script>
    <script src="${rolesURL}"></script>
    <script src="${modifyURL}"></script>
</head>

<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <p class="navbar-text">Profile</p>
        <ul class="nav nav-pills">
            <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_SUPER_ADMIN')">
                <a class="btn btn-primary" href="/user/search">User search</a>
            </sec:authorize>
            <button type="button" class="btn btn-default navbar-btn">
                <a href="<c:url value="/account/logout"/>">Logout</a></button>
        </ul>
    </div>
</nav>

<div class="container" id="main">
    <div class="row" id="update">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Profile</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Email:</td>
                                    <td>
                                        <div class="form-control input-sm" id="1"><c:out value="${user.userEmail}"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="firstName">Name:</td>
                                    <td>
                                        <div class="form-control input-sm inp" id="4"><c:out value="${user.userFirstName}"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="lastName">SurName:</td>
                                    <td>
                                        <div class="form-control input-sm inp" id="5"><c:out value="${user.userLastName}"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="zip">ZIP-code:</td>
                                    <td>
                                        <div class="form-control input-sm inp" id="6"><c:out value="${user.userZIPCode}"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Phone Number:</td>
                                    <td>
                                        <div class="form-control input-sm" id="7"><c:out value="${user.userPhoneNumber}"/></div>
                                    </td>
                                </tr>

                                <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_SUPER_ADMIN')">
                                    <tr>
                                        <td>Current role:</td>
                                        <td>
                                            <div class="form-control input-sm selector" id="mainRole"></div>
                                        </td>
                                    </tr>

                                    <script>
                                        <c:forEach items="${user.userRole}" var="role">
                                              addRole("${role.role}");
                                        </c:forEach>
                                        mainRole();
                                    </script>

                                </sec:authorize>

                                <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
                                    <script>
                                        setSessionRole('ROLE_ADMIN');
                                    </script>
                                </sec:authorize>

                                </tbody>
                            </table>
                            <c:if test="${requestScope['javax.servlet.forward.request_uri'] == '/user/account' || requestScope['javax.servlet.forward.request_uri'] == '/user/account/'}">
                                <button type="submit" class="btn btn-sm btn-warning" id="preUpd" onclick="modify()">Update</button>
                            </c:if>
                            <c:if test="${requestScope['javax.servlet.forward.request_uri'] != '/user/account' && requestScope['javax.servlet.forward.request_uri'] != '/user/account/'}">
                                <button type="submit" class="btn btn-sm btn-warning" id="roleUpd" onclick='changeRole("${user.userID}")'>Change role</button>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
