<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>User search</title>
    <meta charset="utf-8">
    <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jqueryURL"/>
    <spring:url value="/resources/js/userSearch.js" var="searchURL"/>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="cssURL"/>
    <spring:url value="/resources/js/search-panel.js" var="panelURL"/>
    <script src="${jqueryURL}"></script>
    <link href="${cssURL}" rel="stylesheet"/>
    <script src="${searchURL}"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="${panelURL}"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body onload="init()">
    <nav class="navbar navbar-default" role="navigation">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <p class="navbar-text">Search</p>
            <ul class="nav nav-pills">
                <a href="/user/account" class="btn btn-primary navbar-btn">Back to account</a>
            </ul>
        </div>
    </nav>

    <div class="container" id ="search-line">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
    		    <div class="input-group">
                    <div class="input-group-btn search-panel">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        	<span id="search_concept">Filter by</span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#Phone">Phone</a></li>
                          <li><a href="#Email">Email</a></li>
                          <li class="divider"></li>
                          <li><a href="#all">Anything</a></li>
                        </ul>
                    </div>
                    <input type="hidden" name="search_param" value="all" id="search_param">
                    <input type="text" class="form-control" name="x" id="complete-field" placeholder="Search term, min 2 symbols" onkeyup="doCompletion()">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" onclick="doCompletion()"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </div>
    	</div>
    </div>
    <div class="container">
      <h2>List of users</h2>
        <table class="table table-bordered table-hover" id="complete-table" name="users-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Phone number</th>
                    <th>ZIP-code</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td id="userFirstLastName" name="userFirstLastName">
                            <a href="account/${user.userID}">
                                <c:out value="${user.userFirstName}"/> <c:out value="${user.userLastName}"/></a>
                        </td>
                        <td id="userEmail" name="userEmail"><c:out value="${user.userEmail}"/></td>
                        <td id="userPhoneNumber" name="userPhoneNumber"><c:out value="${user.userPhoneNumber}"/></td>
                        <td id="userZIPCode" name="userZIPCode"><c:out value="${user.userZIPCode}"/></td>
                        <td name="user-item">
                            <button type="submit" class="btn btn-danger" id="${user.userID}"
                                    onclick="deleteRow(this.getAttribute('id'), this.parentNode.parentNode.rowIndex)">Delete</button>

                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</body>
</html>
