<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jqueryURL"/>
    <spring:url value="/resources/js/authentication.js" var="authURL"/>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="cssURL"/>
    <link href="${cssURL}" rel="stylesheet"/>
    <script src="${jqueryURL}"></script>
    <script src="${authURL}"></script>
    <title>Remember Password</title>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <p class="navbar-text">Remember Password</p>
            <ul class="nav navbar-nav">
                <li><a href="/user/registerMe">Back to register</a></li>
            </ul>
        </div>
    </nav>

    <div class="row" id="auth">
        <div class="col-md-4 col-md-offset-4">
            <c:if test="${not empty error}">
                <div class="alert alert-danger" role="alert">${error}</div>
            </c:if>
            <div class="panel panel-info" id="panel">
                <div class="panel-heading" id="panelHeader">Enter your email and phone number</div>
                <div class="panel-body">
                    <form role="form" method="POST" action="/user/sendPassword">
                        <div class="form-group" id="1">
                            <label for="Email" id="EmailLabel">Email</label>
                            <input name="email" type="email" class="form-control input-sm" id="Email" placeholder="Example: name@domain.com"/>
                            <span class="glyphicon form-control-feedback" id="MailSpan"></span>
                        </div>
                        <div class="form-group" id="2">
                            <label  for="Name" id="NameLabel">First name</label>
                            <input name="name" type="text" class="form-control input-sm" id="Name" placeholder="Example: Nikolay"/>
                            <span class="glyphicon form-control-feedback" id="NameSpan"></span>
                        </div>
                        <div class="form-group" id="7">
                            <label  for="Phone" id="PhoneLabel">Phone number</label>
                            <input name="phone" type="text" class="form-control input-sm" id="Phone" placeholder="Example: 89173877423" maxlength="11"/>
                            <span class="glyphicon form-control-feedback" id="PhoneSpan"></span>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Send new password</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
