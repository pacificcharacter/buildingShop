<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta charset="utf-8">
    <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jqueryURL"/>
    <spring:url value="/resources/js/authentication.js" var="authURL"/>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="cssURL"/>
    <link href="${cssURL}" rel="stylesheet"/>
    <script src="${jqueryURL}"></script>
    <script src="${authURL}"></script>
    <title>You are not allowed</title>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <p class="navbar-text">404</p>
            <ul class="nav navbar-nav">
                <li class="previous"><a href="/user/account">&larr; Back to account</a></li>
            </ul>
        </div>
    </nav>
    <div class="jumbotron">
        <h1>Oh, 404!</h1>
        <p>Sorry, the requested resource is not found :(</p>
    </div>
</body>
</html>
