<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta charset="utf-8">
    <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jqueryURL"/>
    <spring:url value="/resources/js/authentication.js" var="authURL"/>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="cssURL"/>
    <link href="${cssURL}" rel="stylesheet"/>
    <script src="${jqueryURL}"></script>
    <script src="${authURL}"></script>
    <title>You are not allowed</title>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <p class="navbar-text">Oh</p>
            <ul class="nav navbar-nav">
                <li><a href="/user/account">Back to account</a></li>
            </ul>
        </div>
    </nav>

    <div class="jumbotron">
        <h1>Oh, 403!</h1>
        <p>Sorry, but you are not allowed :(</p>
    </div>
</body>
</html>
