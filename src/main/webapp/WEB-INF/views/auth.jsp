<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jqueryURL"/>
    <spring:url value="/resources/js/authentication.js" var="authURL"/>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="cssURL"/>
    <link href="${cssURL}" rel="stylesheet"/>
    <script src="${jqueryURL}"></script>
    <script src="${authURL}"></script>
    <title>Authentication</title>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <p class="navbar-text">Sign In</p>
            <ul class="nav navbar-nav">
                <li><a href="/user/registerMe">Register</a></li>
            </ul>
        </div>
    </nav>

    <div class="row" id="auth">
        <div class="col-md-4 col-md-offset-4">
            <c:if test="${not empty error}">
                <div class="alert alert-danger" role="alert">${error}</div>
            </c:if>
            <c:if test="${not empty msg}">
                <div class="alert alert-info" role="alert">${msg}</div>
            </c:if>
            <div class="panel panel-info" id="panel">
                <div class="panel-heading" id="panelHeader">Enter your email and password</div>
                <div class="panel-body">
                    <c:url value="/j_spring_security_check" var="loginUrl" />
                    <form role="form" method="POST" action="${loginUrl}">
                        <div class="form-group" id="1">
                            <label  for="Email" id="EmailLabel">Email</label>
                            <input name="j_username" type="email" class="form-control input-sm" id="Email" placeholder="Example: name@domain.com"/>
                            <span class="glyphicon form-control-feedback" id="MailSpan"></span>
                        </div>
                        <div class="form-group" id="2">
                            <label  for="Password" id="PasswordLabel">Password</label>
                            <input name="j_password" type="password" class="form-control input-sm" id="Password" placeholder="Enter password"/>
                            <span class="glyphicon form-control-feedback" id="PasswordSpan"></span>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm" id="submitThis">Sign Up</button>
                        <button type="button" class="btn btn-default navbar-btn btn-sm"><a href="/user/forgotPassword">
                            Forgot password?</a></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
