<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jqueryURL"/>
    <spring:url value="/resources/js/register.js" var="registerURL"/>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="cssURL"/>
    <link href="${cssURL}" rel="stylesheet"/>
    <script src="${jqueryURL}"></script>
    <script src="${registerURL}"></script>

    <title>Register now!</title>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <p class="navbar-text">Creating a new user</p>
        <ul class="nav navbar-nav">
            <li><a href="/user/">Login Now!</a></li>
        </ul>
    </div>
</nav>

<div class="row" id="auth">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-info">
            <div class="panel-heading">Fill in all fields</div>
            <div class="panel-body">
                <sf:form role="form" method="POST" modelAttribute="user" action="/user/register">
                    <div class="form-group" id="1">
                        <label  for="Email" id="EmailLabel">Email</label>
                        <sf:input path="userEmail" name="email" type="email" class="form-control input-sm" id="Email" placeholder="Example: name@domain.com"/>
                        <span class="glyphicon form-control-feedback" id="MailSpan"></span>
                    </div>
                    <div class="form-group" id="2">
                        <label  for="Password" id="PasswordLabel">Password</label>
                        <sf:input path="userPassword" name="password" type="password" class="form-control input-sm" id="Password" placeholder="Enter password"/>
                        <span class="glyphicon form-control-feedback" id="PasswordSpan"></span>
                    </div>
                    <div class="form-group" id="3">
                        <label  for="Confirm" id="ConfirmLabel">Confirm password</label>
                        <input name="confirm" type="password" class="form-control input-sm" id="Confirm" placeholder="Enter password again">
                        <span class="glyphicon form-control-feedback" id="ConfirmSpan"></span>
                    </div>
                    <div class="form-group" id="4">
                        <label  for="Name" id="NameLabel">First name</label>
                        <sf:input path="userFirstName" name="name" type="text" class="form-control input-sm" id="Name" placeholder="Example: Nikolay"/>
                        <span class="glyphicon form-control-feedback" id="NameSpan"></span>
                    </div>
                    <div class="form-group" id="5">
                        <label for="Lastname" id="LastnameLabel">Last name</label>
                        <sf:input path="userLastName" name="lastname" type="text" class="form-control input-sm" id="Lastname" placeholder="Example: Programmer"/>
                        <span class="glyphicon form-control-feedback" id="LastnameSpan"></span>
                    </div>
                    <div class="form-group" id="6">
                        <label  for="ZIP" id="ZIPLabel">ZIP-code</label>
                        <sf:input path="userZIPCode" name="zip" type="text" class="form-control input-sm" id="ZIP" placeholder="Example: 678111" maxlength="6"/>
                        <span class="glyphicon form-control-feedback" id="ZIPSpan"></span>
                    </div>
                    <div class="form-group" id="7">
                        <label  for="Phone" id="PhoneLabel">Phone number</label>
                        <sf:input path="userPhoneNumber" name="phone" type="text" class="form-control input-sm" id="Phone" placeholder="Example: 89173877423" maxlength="11"/>
                        <span class="glyphicon form-control-feedback" id="PhoneSpan"></span>
                    </div>
                    <sf:button type="submit" class="btn btn-success btn-sm" id="submitThis">Sign Up</sf:button>
                </sf:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
